require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test 'Product attributes must not be empty'  do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:image_url].any?
    assert product.errors[:price].any?
  end  # end

  test 'Price is greater than 0.01' do
    product = Product.new(title: "my book",
                          description: "mmm",
                          image_url: "xx.jpg")
    product.price = -1
    assert product.invalid?
    assert_equal ['must be greater than or equal to 0.01'], product.errors[:price]
    product.price = 0
    assert product.invalid?
    assert_equal ['must be greater than or equal to 0.01'], product.errors[:price]

    product.price = 1
    assert product.valid?
  end

  def new_product(image_url)
    product = Product.new(title: "my book",
                          description: "mmm",
                          image_url: image_url)
  end

  test 'Image url' do
    ok = %w{ fred.gif fred.jpg fred.png FRED.JPG FRED.Jpg
             http://a.b.c/x/y/z/fred.jpg}
    bad = %w{ fred.doc fred/more fred.gif.more}

    ok.each do |name|
      assert new_product(name).valid?, "#{name} shold be valid"
    end

    bad.each do |name|
      assert new_product(name).invalid?, "#{name} shold be not valid"
    end
  end

  test "Unique title - il8n" do
    product = Product.new(title: products(:ruby).title,
                          description: "mmm",
                          image_url: "xx.jpg")
    assert product.invalid?
    assert_equal [I18n.translate('errors.messages.taken')],
                  product.errors[:title]
  end
end
